\documentclass{article}

\usepackage{booktabs}
\usepackage{lmodern} % change font to Latin Modern
\usepackage{qrcode}
\usepackage{tabularx}
\usepackage{pdfpages}
\usepackage{xfrac}
\newcolumntype{L}{>{\raggedright}X}
\usepackage{titlesec}
\usepackage{framed}
\usepackage{hyperref}
\usepackage{calc}

% -- date macros --
\def\semester/{Spring 2020}
\def\finalexamdate/{Tuesday, May 12th, 4:00 - 5:50 p.m.}
\def\basicskillsclassdate/{Friday, March 12th}
\def\basicskillsstartdate/{Monday, March 16th}
\def\basicskillsenddate/{Monday, April 13th}

% -- url macros --
\def\myemail/{pmodlin@gus.pittstate.edu}
\def\syllabusurl/{https://drive.google.com/open?id=1AN2oy9hQPg7uV8qY6mNHNSI8lbtZdntM}
\def\academichonestyurl/{https://www.pittstate.edu/registrar/catalog/current/index.html}

\title{
	\semester/ \\
	MATH 113 -- College Algebra
}
% using \author as subtitle
\author{
	Course Syllabus \\
}
\date{} % remove date

\begin{document}

\maketitle

% heading information
\noindent
\begin{tabularx}{\textwidth}{r L r l}
	\midrule
	Instructor: & Paul Modlin &
	Office: & 212 Yates \\
	Email: & \href{mail-to:\myemail/}{\myemail/} &
	Tutor room: & 223 Yates \\
	Office hours: & Mon 2-3pm, Tues 1-3pm Wed 2-3pm, Thurs 2-3pm&
	Tutor hours: & TBA \\
	\midrule
\end{tabularx}

% remove section numbers
\titleformat{\section}{\normalfont\large\bfseries}{}{0em}{}
\titleformat{\subsection}{\normalfont\bfseries}{}{0em}{}

\section{General Education}
This course counts toward the requirements in General Education for your degree program.
General Education is an important part of your educational program at PSU that has been designed to implement the following philosophy:

\begin{quote}
General education is the study of humans in their global setting.
The general education curriculum, therefore, acts as the heart of a university education by developing the capacities that typify the educated person and providing a basis for like-long learning and intellectual, ethical, and aesthetic fulfillment.
General education examines the world around us and fosters an understanding of our interactions with the world and our place in the universe.
General education celebrates the creative capacities of humankind and helps to preserve and transmit to future generations the values, knowledge, wisdom, and sense of history that are our common heritage.
\end{quote}
	
\section{Goals of General Education for this Course}
This course will help you to accomplish several of the Goals and Objectives of General Education,
including the development of your ability to use the tools of mathematics to communicate and to formulate and solve problems (I.3),
the development of your critical thinking skills (II.1-4)
and the development of your knowledge as to how mathematics has affected society,
its institutions,
and the world as a whole (IV.iv.1, 2 and IV.vii.1, 2).

A special emphasis of this course is to help you to fulfill the following objectives (I.3 and II.1-4).  Upon successful completion of this course, you will be able to:
\begin{itemize}

	\item Demonstrate the ability to formulate and solve problems using the tools of mathematics.
	\item Demonstrate the ability to distinguish between relevant and irrelevant information in problem solving.
	\item Articulate a problem and develop a logical and reasonable response to it using appropriate sources.
	\item Apply generalizations, principles, theories, or rules to the real world.
	\item Demonstrate the ability to analyze and synthesize information.

\end{itemize}

\section{Prerequisite}
MATH 019 Intermediate Algebra or $1 \sfrac 1 2$ units of high school algebra.

\section{Course Description}
This course is designed for the students to learn traditional college algebra concepts and problem solving skills.
It should serve to prepare students not only for their higher studies in math but also to use and appreciate the concepts of algebra in day-to-day activities.
The topics include: operations with algebraic expressions; linear and quadratic functions; graphs of polynomial and rational functions; systems of equations; logarithmic and exponential functions; arithmetic and geometric progressions; permutations and combinations.

Closed to students with credit in MATH 110 College Algebra with Review or MATH 126 Pre-Calculus or MATH 153 Introduction to Analytic Processes, or students with a letter grade of ``C'' or better in MATH 150 Calculus I.

\section{Instructional Resources}

\begin{itemize}
	\item This course \textbf{requires} access to MyMathLab for completing homework.
		See the last page of this syllabus for a MyMathLab registration guide.
	\item This course uses the text \emph{College Algebra (Fifth Edition)} by Robert Blitzer.
		A physical copy of the textbook is \textbf{not required};
		MyMathLab access includes access to the textbook.
	\item 223 Yates Hall is available as a tutor room for your use free of charge.
		A list of tutors and their schedules are posted on the door.
\end{itemize}

\section{Objectives}
The student is expected to do the following:

\begin{itemize}
	\item Review the fundamental concepts of the real numbers and algebraic expressions including rational expressions
	\item Develop a vocabulary of algebraic terms and symbols
	\item Develop problem-solving techniques for linear and quadratic equations and inequalities
	\item Develop graphing skills and interpretation of graphs by algebraic and technological methods
	\item Develop skills related to functions and matrices
	\item Develop an understanding of exponential and logarithmic functions
	\item Develop skills used in solving systems of equations
	\item Develop an understanding of how the above objectives apply to and can model real world situations in various disciplines
	\item Develop an understanding of the processes and results of algebraic transformations
\end{itemize}

\section{Method of Evaluation}

\subsection{Tests}
There will be 4 or 5 unit tests, in addition to the comprehensive final exam.
You may be asked to put bags, phones, PDAs, and smart devices away to one side of the room.
These tests will be based on the text, in-class lectures, homework, and in-class projects.
Your unit tests will be weighted as 40\% of your final grade.
Each student's lowest-scoring test will be dropped from your final grade.
The comprehensive final exam will be weighted as 20\% of your final grade.
Make-up tests will not be given, but each student's lowest unit test score will be dropped.

\textbf{The comprehensive final exam will be \finalexamdate/.}
There will be a test during the week prior to finals.

\subsection{Quizzes}

There will be pop quizzes and in-class group assignments to be written up and turned in for a grade.
Pop quizzes and in-class group work \textbf{cannot be made up}.
The lowest-scoring two quizzes will be dropped from your final grade.
Quizzes and group work will be weighted to count for 20\% of your final grade.

\subsection{Homework}
Homework will be given each class session on MyMathLab and/or from the textbook.
In MyMathLab the ``Do Homework'' button will take you to the actual homework problems
which may be worked and reworked to improve your score.
You are free and encouraged to work more problems than those assigned:
navigate to ``Study Plan'' where problems can be selected by chapter, section, or topic.
Work from ``Study Plan'' will \textbf{not affect your grade}.
\textbf{Homework not done by the due date will be scored as a zero.}
Homework will be weighted to count for 20\% of your final grade.

Generally, you should expect to spend at least two hours working outside of class for each hour in class;
``homework'' includes studying the text as well as solving problems.

\subsection{Final Grade}
The final grade will be based on a percentage of total points from hourly exams, quizzes, homework, and the final.

In summary, the final grade will be weighted as follows:
\vspace{1ex}

\noindent
\begin{tabularx}{\textwidth}{X X X X}
	\midrule
	Unit Tests & Final Exam & Quizzes & Homework \\
	40\% & 20\% & 20\% & 20\% \\
	\midrule
\end{tabularx}

The grading scale is:
\begin{center}
	\begin{tabular}{r r}
	\midrule
		90 - 100\% & A \\
		80 - 89\% & B \\
		70 - 79\% & C \\
		60 - 69\% & D \\
		Below 60\% & F \\
	\midrule
	\end{tabular}
\end{center}

\section{Basic Skills Exam}
To receive credit in this course you MUST score at least 9 out of 11 correct on a basic skills examination.
The skills tested are required for success in the mathematics courses for which this course is a prerequisite.
Your score on this test will not be counted in your course grade.
The exam will be given in class on \basicskillsclassdate/.
You may take the test at times scheduled from \basicskillsstartdate/ through \basicskillsenddate/ (the last day to drop a course).
You may retake different versions of the test until you pass the exam.
If you have not passed the exam by \basicskillsenddate/, you must either drop the course or else receive a failing grade.

The purposes of this test include:
(1) to motivate mastery of basic pencil and paper skills;
(2) to indicate a high degree of accuracy in performing basic skills.

\section{Other Resources}

% version will be raised if necessary
% level will be raised if possible
\qrset{link,height=3.5cm,version=3,level=L}
\renewcommand{\arraystretch}{4}
\begin{tabularx}{\textwidth}{L r}

	This syllabus can be found online at \url{\syllabusurl/} &
	\raisebox{3ex - \height}{
		\qrcode[]{\syllabusurl/}
		          
	} \\

	The \semester/ academic policies and regulations can be found at
	\url{\academichonestyurl/} &
	\raisebox{3ex - \height}{
		\qrcode[]{\academichonestyurl/}
	} \\

	The \semester/ Syllabi Supplement, a ``one-stop'' place for students to access up-to-date information about campus resources, can be found at
	\url{https://www.pittstate.edu/registrar/syllabus-supplement.html} &
	\raisebox{3ex - \height}{
		\qrcode[]{https://www.pittstate.edu/registrar/syllabus-supplement.html}
	}

\end{tabularx}

\includepdf[pages=-]{mymathlab_info.pdf}
\end{document}
